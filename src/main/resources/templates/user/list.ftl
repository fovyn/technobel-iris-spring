<#import '../layout.ftl' as layout>

<#assign userManagment>
    <script>

    </script>
</#assign>

<@layout.layout titre="Users" js=userManagment>
    <a href="/user/register">Ajouter un utilisateur</a>
    <table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Lastname</th>
                <th>Firstname</th>
                <th>Email</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        <#list users as user>
            <tr>
                <td>${user.id}</td>
                <td>${user.lastName}</td>
                <td>${user.firstName}</td>
                <td>${user.email}</td>
                <td>
                    <a class="btn" href="/user/profile/${user.email}">Profile</a>
                </td>
            </tr>
        </#list>
        </tbody>
    </table>
</@layout.layout>
