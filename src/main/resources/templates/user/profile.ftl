<#import "../layout.ftl" as layout>

<@layout.layout titre="User - Profile">
    <form action="/user/profile/edit/${model.email}" method="post">
        <div class="input-field">
            <label for="lastName">Lastname</label>
            <input name="lastName" id="lastName" type="text" value="${model.lastName}">
        </div>
        <div class="input-field">
            <label for="firstName">Firstname</label>
            <input name="firstName" id="firstName" type="text" value="${model.firstName}">
        </div>
        <div class="input-field">
            <label for="email">Email</label>
            <input name="email" id="email" type="email" value="${model.email}">
        </div>
        <div class="input-field">
            <label for="password">Password</label>
            <input name="password" id="password" type="password">
        </div>
        <button type="submit">Ajouter</button>
    </form>
</@layout.layout>
