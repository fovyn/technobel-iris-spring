<#import '../layout.ftl' as layout>

<#assign userManagment>
    <script>

    </script>
</#assign>

<@layout.layout titre="User - Register" js=userManagment>
    <form action="/user/register" method="post">
        <div class="input-field">
            <label for="lastName">Lastname</label>
            <input name="lastName" id="lastName" type="text">
        </div>
        <div class="input-field">
            <label for="firstName">Firstname</label>
            <input name="firstName" id="firstName" type="text">
        </div>
        <div class="input-field">
            <label for="email">Email</label>
            <input name="email" id="email" type="email">
        </div>
        <div class="input-field">
            <label for="password">Password</label>
            <input name="password" id="password" type="password">
        </div>
        <div class="input-field">
            <label for="passwordConfirm">Confirm password</label>
            <input name="passwordConfirm" id="passwordConfirm" type="password">
        </div>
        <button type="submit">Ajouter</button>
    </form>
</@layout.layout>
