package be.technobel.formation.iris.spring.model.view.user;

import be.technobel.formation.iris.spring.toolbox.ConfirmPassword;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class RegisterDTO {
    @NotNull
    @NotBlank
    private String lastName;
    @NotNull
    @NotBlank
    private String firstName;
    @NotNull
    @NotBlank
    @Email
    private String email;

    @Size(min = 5)
    private String password;
    @Size(min = 5)
    private String confirmPassword;
}
