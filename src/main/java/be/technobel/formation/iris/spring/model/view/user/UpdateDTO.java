package be.technobel.formation.iris.spring.model.view.user;

import lombok.Data;
import org.checkerframework.common.value.qual.MinLen;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class UpdateDTO {
    @NotNull
    @NotBlank
    private String lastName;
    @NotNull
    @NotBlank
    private String firstName;
    @NotNull
    @NotBlank
    @Email
    private String email;
    @NotBlank
    @NotNull
    @MinLen(value = 5)
    private String password;
}
