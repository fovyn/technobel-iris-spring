package be.technobel.formation.iris.spring.model.view.home;

import lombok.Data;
import lombok.Getter;

import java.util.List;

@Data
public class IndexView {
    private String username;

    private List<Float> cotes;
}
