package be.technobel.formation.iris.spring.toolbox;

import be.technobel.formation.iris.spring.model.entity.User;
import be.technobel.formation.iris.spring.model.view.user.RegisterDTO;
import be.technobel.formation.iris.spring.model.view.user.UpdateDTO;

import java.lang.reflect.Field;

public class Mapper {
    public static  <TValue> void map(TValue src1, TValue src2) throws IllegalAccessException {
        Field[] fields = src1.getClass().getDeclaredFields();

        for(Field field: fields) {
            field.setAccessible(true);
            Object value1 = field.get(src1);
            Object value2 = field.get(src2);

            if (value1 != null && value2 != null && !value1.equals(value2)) {
                field.set(src1, value2);
            } else if (value1 == null && value2 != null) {
                field.set(src1, value2);
            }
        }
    }

    public static User map(RegisterDTO registerDTO) {
        User user = new User();
        user.setFirstName(registerDTO.getFirstName());
        user.setLastName(registerDTO.getLastName());
        user.setEmail(registerDTO.getEmail());
        user.setPassword(registerDTO.getPassword());

        return user;
    }

    public static User map(UpdateDTO updateDTO) {
        User user = new User();
        user.setLastName(updateDTO.getLastName());
        user.setFirstName(updateDTO.getFirstName());
        user.setEmail(updateDTO.getEmail());
        user.setPassword(updateDTO.getPassword());

        return user;
    }

    public static void merge(User target, User src) {
        target.setFirstName(src.getFirstName());
        target.setLastName(src.getLastName());
        target.setPassword(src.getPassword() != null && !src.getPassword().equals("") ? src.getPassword() : target.getPassword());
        target.setEmail(src.getEmail());
    }
}
