package be.technobel.formation.iris.spring.controller;

import be.technobel.formation.iris.spring.model.view.user.RegisterDTO;
import be.technobel.formation.iris.spring.model.entity.User;
import be.technobel.formation.iris.spring.model.view.user.UpdateDTO;
import be.technobel.formation.iris.spring.service.user.UserService;
import be.technobel.formation.iris.spring.toolbox.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.nio.MappedByteBuffer;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(path = "/user")
public class UserController {
    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(path = {"/all", "/list", "/", ""}, name = "user_list")
    public String listAction(Map<String, List<User>> model) {
        model.put("users", this.userService.readAll());
        return "user/list";
    }

    @GetMapping(path = {"/{email}", "/profile/{email}"}, name = "user_profile")
    public String profileAction(Map<String, User> model, @PathVariable String email) {
        User u = this.userService.findOneByEmail(email);
        model.put("model", u);
        return "user/profile";
    }

    @PostMapping(path = {"/email", "/profile/edit/{email}", "/edit/{email}"})
    public String updateAction(@PathVariable String email, @Valid UpdateDTO updateDTO) {
        this.userService.update(email, Mapper.map(updateDTO));
        return "redirect:/user/list";
    }

    @GetMapping(path = {"/register"}, name = "user_register_get")
    public String registerAction() {
        return "user/register";
    }
    @PostMapping(path= {"/register"}, name = "user_register_post")
    public String registerAction(@Valid RegisterDTO registerDTO) {
        this.userService.create(Mapper.map(registerDTO));

        return "redirect:/user/list";
    }
}
