package be.technobel.formation.iris.spring.repository;

import be.technobel.formation.iris.spring.model.entity.Role;
import be.technobel.formation.iris.spring.model.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleRepository extends CrudRepository<Role, Long> {
}
