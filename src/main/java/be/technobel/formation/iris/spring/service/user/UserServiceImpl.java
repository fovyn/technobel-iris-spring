package be.technobel.formation.iris.spring.service.user;

import be.technobel.formation.iris.spring.model.entity.User;
import be.technobel.formation.iris.spring.repository.UserRepository;
import be.technobel.formation.iris.spring.toolbox.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository repository;

    @Autowired
    public UserServiceImpl(UserRepository repository) {
        this.repository = repository;
    }

    @Override
    public User create(User user) {
        return this.repository.save(user);
    }

    @Override
    public List<User> readAll() {
        return this.repository.findAll();
    }

    @Override
    public User findOneByEmail(String email) {
        return this.repository.findOneByEmail(email).orElse(null);
    }

    @Override
    public void update(String email, User fromDto) {
        Optional<User> optUser = this.repository.findOneByEmail(email);

        if (optUser.isPresent()){
            User user = optUser.get();
            Mapper.merge(user, fromDto);
            this.repository.save(user);
        }
    }
}
