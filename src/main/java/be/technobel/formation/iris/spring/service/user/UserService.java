package be.technobel.formation.iris.spring.service.user;

import be.technobel.formation.iris.spring.model.entity.User;

import java.util.List;

public interface UserService {
    User create(User user);
    List<User> readAll();
    User findOneByEmail(String lastname);

    void update(String username, User fromDto);
}
